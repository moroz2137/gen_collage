import React from "react";
import Modal from "./Modal";
import { useStateReducer } from "../store/useStateReducer";
import { ActionType } from "../store/AppReducerAction";
import { FileRejection } from "react-dropzone";

const formatErrorMessage = ({ errors: [{ code, message }] }: FileRejection) => {
  switch (code) {
    case "file-invalid-type":
      return `The image must be in JPEG or PNG format.`;
    case "file-too-large":
      return `The image is larger than 10 MB.`;
    default:
      return message;
  }
};

const FileRejectedModal = () => {
  const [state, dispatch] = useStateReducer();
  const files = state.rejectedFiles;
  const onClose = () =>
    dispatch({
      type: ActionType.SetRejectedFiles,
      value: []
    });
  return (
    <Modal onClose={onClose}>
      <p>A file you have tried to upload has been rejected:</p>
      {files.map((file, index) => (
        <p key={index}>
          <strong>{file.file.name}</strong>: {formatErrorMessage(file)}
        </p>
      ))}
    </Modal>
  );
};

export default FileRejectedModal;
