import React from "react";
import style from "./Modal.module.sass";
import Button from "../Button";

interface Props {
  onClose(): void;
  children: any;
}

const Modal = ({ onClose, children }: Props) => {
  return (
    <div className={style.modal}>
      {children}
      <Button onClick={onClose}>Close</Button>
    </div>
  );
};

export default Modal;
