import React from "react";
import style from "./ColorPicker.module.sass";
import cs from "classnames";

interface Props {
  value: string;
  label: string;
  selected: boolean;
  onClick(e: any): void;
}

function hexToRgb(hex: string) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      }
    : { r: 227, g: 20, b: 134 };
}

function shadowForColor(hex: string) {
  const { r, g, b } = hexToRgb(hex);
  return `0 0 5px 3px rgba(${r}, ${g}, ${b}, .5)`;
}

const Swatch = ({ value, selected, label, onClick }: Props) => {
  const boxShadow = selected ? shadowForColor(value) : "none";
  return (
    <div
      className={cs({
        [style.swatch]: true,
        [style.swatchActive]: selected
      })}
      onClick={onClick}
      style={{ backgroundColor: value, boxShadow }}
      title={label}
    />
  );
};

export default Swatch;
