import React from "react";
import style from "./ColorPicker.module.sass";
import Swatch from "./Swatch";

interface Props {
  value: string | null;
  onChange(value: string): () => void;
  options: Record<string, string>;
  label: string;
  labelClass?: string;
}

export default ({ options, value, onChange, label, labelClass }: Props) => {
  return (
    <div className={style.colorPicker}>
      <h2 className={`editor__large_label ${labelClass || ""}`}>{label}</h2>
      <div className={style.swatches}>
        {Object.keys(options).map((key) => (
          <Swatch
            key={key}
            value={options[key]}
            label={key}
            selected={options[key] === value}
            onClick={onChange(options[key])}
          />
        ))}
      </div>
    </div>
  );
};
