import React from "react";
import { Helmet } from "react-helmet";

interface Props {
  children?: string;
}

const DEFAULT_TITLE = `gen_collage`;

const PageTitle = ({ children }: Props) => {
  const title = children ? `${children} | ${DEFAULT_TITLE}` : DEFAULT_TITLE;

  return (
    <Helmet>
      <title>{title}</title>
    </Helmet>
  );
};
export default PageTitle;
