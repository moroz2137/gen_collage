import React from "react";
import style from "../editor/Editor.module.sass";

interface Props {
  children: any;
}

const PaddedContainer = ({ children }: Props) => {
  return <div className={style.paddedContainer}>{children}</div>;
};

export default PaddedContainer;
