import React from "react";
import { Link } from "react-router-dom";
import LogoImg from "./logo.png";
import style from "./NavigationHeader.module.sass";

const NavigationHeader = () => {
  return (
    <header className={`${style.navigationHeader} container`}>
      <Link to="/" className={style.branding}>
        <img src={LogoImg} alt="ArtPlanet logo" />
      </Link>
    </header>
  );
};

export default NavigationHeader;
