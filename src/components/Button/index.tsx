import React, { MouseEvent } from "react";
import cs from "classnames";
import { Link } from "react-router-dom";

interface Props {
  fullwidth?: boolean;
  stroke?: boolean;
  children: any;
  href?: string;
  black?: boolean;
  className?: string;
  onClick?(e: MouseEvent<HTMLButtonElement>): any;
}

const Button = ({
  fullwidth,
  children,
  onClick,
  href,
  stroke,
  black,
  className
}: Props) => {
  const classNames = cs({
    button: true,
    "is-fullwidth": fullwidth,
    stroke: stroke,
    "is-black": black
  });
  if (href) {
    return (
      <Link to={href} className={`${classNames} ${className || ""}`}>
        {children}
      </Link>
    );
  }
  return (
    <button className={`${classNames} ${className || ""}`} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
