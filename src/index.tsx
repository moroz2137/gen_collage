import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import PyofRoutes from "./routes";

ReactDOM.render(
  <React.StrictMode>
    <PyofRoutes />
  </React.StrictMode>,
  document.getElementById("pyof")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
