import React from "react";
import "./css/App.sass";
import { useStateReducer } from "./store/useStateReducer";
import Header from "./editor/Header";

function App() {
  const [state] = useStateReducer();
  return (
    <div className="editor">
      <Header />
      <pre>{JSON.stringify(state, null, 2)}</pre>
    </div>
  );
}

export default App;
