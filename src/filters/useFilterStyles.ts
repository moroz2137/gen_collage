import { CSSProperties, useMemo } from "react";
import FilterFactory from "./FilterFactory";
import FilterPresets from "./presets";

interface FilterStyles {
  overlay: CSSProperties;
  filter: CSSProperties;
}

export default (key: string | null): FilterStyles => {
  return useMemo(() => {
    if (key === null)
      return {
        overlay: {
          display: "none"
        },
        filter: {}
      };

    const preset = FilterPresets[key];
    const factory = new FilterFactory(preset.filter, preset.overlay);
    return {
      filter: factory.getFilterStyles() as CSSProperties,
      overlay: factory.getOverlayStyles() as CSSProperties
    };
  }, [key]);
};
