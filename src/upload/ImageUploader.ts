const ENDPOINT =
  process.env.NODE_ENV === "development"
    ? "http://localhost:4000/api/generate_presigned_url"
    : "/api/pyof/generate_presigned_url";

interface GenerateUrlResponse {
  public_url: string;
  upload_url: string;
}

export const generatePresignedUrl = async (
  filename: string,
  prefix: string = "images"
): Promise<GenerateUrlResponse> => {
  const body = JSON.stringify({ filename, prefix });
  const rawRes = await fetch(ENDPOINT, {
    method: "POST",
    body,
    mode: "cors",
    headers: {
      "Content-Type": "application/json"
    }
  });
  const json = await rawRes.json();
  return json;
};

const putFileToS3 = (
  url: string,
  file: File,
  progressCallback: (progress: number) => any
) => {
  return new Promise((resolve) => {
    const xhr = new XMLHttpRequest();

    xhr.upload.addEventListener("progress", function (ev) {
      if (ev.lengthComputable) {
        progressCallback(ev.loaded / ev.total);
      }
    });
    xhr.addEventListener("load", function () {
      resolve(xhr.response);
    });
    xhr.open("PUT", url, true);
    xhr.setRequestHeader(
      "cache-control",
      "public, max-age=31536000, immutable"
    );
    xhr.send(file);
  });
};

export const uploadFile = async (
  file: File,
  prefix: string = "images",
  onProgress: (progress: number) => any
): Promise<string> => {
  const filename = file.name;
  const { public_url, upload_url } = await generatePresignedUrl(
    filename,
    prefix
  );
  await putFileToS3(upload_url, file, onProgress);
  return public_url;
};

export const downloadFile = async (
  url: string,
  onProgress: (progress: number) => any
): Promise<any> => {
  return new Promise((resolve, reject) => {
    var req = new XMLHttpRequest();
    req.open("GET", url, true);
    req.responseType = "arraybuffer";
    req.addEventListener("load", () => {
      if (req.status === 200) {
        const blob = new Blob([req.response]);
        const url = window.URL.createObjectURL(blob);
        resolve(url);
      } else {
        reject(req.response);
      }
    });
    req.addEventListener("progress", (e) => {
      if (e.lengthComputable) {
        onProgress(e.loaded / e.total);
      }
    });
    req.addEventListener("loadstart", () => {
      onProgress(0);
    });
    req.addEventListener("error", () => reject(req.response));
    req.send();
  });
};
