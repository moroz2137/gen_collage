import React, { useMemo } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import EditorView from "../editor/EditorView";
import {
  AppReducerContext,
  useStateReducerProvider
} from "../store/AppReducerContext";
import "../css/App.sass";
import EditTextView from "../editor/EditTextView";
import EditFrameView from "../editor/EditFrameView";
import CollageTypeView from "../editor/CollageTypeView";
import ImageFilterView from "../editor/ImageFilterView";
import CropImageView from "../editor/CropImageView";
import SpecialFrameView from "../editor/SpecialFrameView";
import AnimatedWizard from "./AnimatedWizard";
import PageTitle from "../components/Helmet";

const PyofRoutes = () => {
  const basePath = useMemo(() => {
    return window.location.pathname.match(/^\/pyof/) ? "/pyof" : "/";
  }, []);

  const appReducerContextValue = useStateReducerProvider();
  return (
    <AppReducerContext.Provider value={appReducerContextValue}>
      <Router basename={basePath}>
        <PageTitle />
        <AnimatedWizard />
        <Switch>
          <Route path="/editor" exact component={EditorView} />
          <Route path="/editor/text" exact component={EditTextView} />
          <Route path="/editor/frames/plain" exact component={EditFrameView} />
          <Route
            path="/editor/frames/custom"
            exact
            component={SpecialFrameView}
          />
          <Route path="/editor/filters" exact component={ImageFilterView} />
          <Route path="/editor/filters" exact component={ImageFilterView} />
          <Route path="/editor/filters" exact component={ImageFilterView} />
          <Route path="/editor/filters" exact component={ImageFilterView} />
          <Route path="/editor/collages" exact component={CollageTypeView} />
          <Route path="/editor/crop/:index" exact component={CropImageView} />
        </Switch>
      </Router>
    </AppReducerContext.Provider>
  );
};

export default PyofRoutes;
