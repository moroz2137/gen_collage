import React from "react";
import { Route, Switch, useLocation } from "react-router";
import ChooseCompositionType from "../wizard/ChooseCompositionType";
import ChooseOrientation from "../wizard/ChooseOrientation";
import ChooseSize from "../wizard/ChooseSize";
import Landing from "../wizard/Landing";
import { CSSTransition, TransitionGroup } from "react-transition-group";

const wizardRoutes = [
  "/",
  "/wizard/poster-type",
  "/wizard/size",
  "/wizard/orientation",
  "/editor"
];

const AnimatedWizard = () => {
  const location = useLocation();
  const currentScreen = wizardRoutes.indexOf(location.pathname);
  const { state } = location as any;
  const previousScreen = state ? wizardRoutes.indexOf(state.previousScreen) : 0;
  const animationClassNames =
    currentScreen > previousScreen ? "slide-forward" : "slide-backward";

  return (
    <TransitionGroup
      childFactory={(child) =>
        React.cloneElement(child, {
          classNames: animationClassNames
        })
      }
    >
      <CSSTransition
        key={location.key}
        classNames={animationClassNames}
        timeout={1000}
      >
        <Switch location={location}>
          <Route path="/" exact component={Landing} />
          <Route path="/index.html" exact component={Landing} />
          <Route
            path="/wizard/poster-type"
            exact
            component={ChooseCompositionType}
          />
          <Route path="/wizard/size" exact component={ChooseSize} />
          <Route
            path="/wizard/orientation"
            exact
            component={ChooseOrientation}
          />
        </Switch>
      </CSSTransition>
    </TransitionGroup>
  );
};

export default AnimatedWizard;
