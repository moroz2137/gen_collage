import React, { useMemo } from "react";
import { ActionType } from "../store/AppReducerAction";
import { useStateReducer } from "../store/useStateReducer";
import { CompositionType, Orientation, Size } from "../store/AppReducerState";
import Hero from "./Hero";
import Layout from "./Layout";
import CollageTypes from "../store/CollageTypes";
import WizardThumbnail from "./WizardThumbnail";
import Landscape from "../images/pejzaz.jpg";
import SmilingLabrador from "../images/smiling-labrador.jpg";
import FlyingLabrador from "../images/labrador-w-locie.jpg";
import Corgi from "../images/corgi.jpg";
import PageTitle from "../components/Helmet";
import BackButton from "./BackButton";
import NextButton from "./NextButton";
import WizardOptionButton from "./WizardOptionButton";

const ChooseSize = () => {
  const [state, dispatch] = useStateReducer();

  const value = state.size;

  const onSelect = (value: Size) => () => {
    dispatch({
      type: ActionType.SetSize,
      value
    });
    // navigateWithState("/wizard/orientation");
  };

  const compositionType = state.composition.type;
  const images = useMemo(
    () =>
      compositionType === CompositionType.Collage
        ? [Corgi, FlyingLabrador, SmilingLabrador]
        : [Landscape],
    [compositionType]
  );
  const collageType =
    compositionType === CompositionType.Collage ? CollageTypes[1] : null;

  return (
    <Layout>
      <PageTitle>Select size</PageTitle>
      <Hero simpleViewOnMobile>
        <div
          className="hero__images"
          style={{ top: ".5rem", "--size-scale": 0.9 } as React.CSSProperties}
        >
          <WizardThumbnail
            size={Size.M}
            orientation={Orientation.Portrait}
            images={images}
            active={value === Size.M}
            composition={{
              collageType,
              type: compositionType
            }}
            label={
              <>
                M Size
                <br />
                49cm x 35cm
              </>
            }
            onClick={onSelect(Size.M)}
          />
          <WizardThumbnail
            size={Size.L}
            orientation={Orientation.Portrait}
            images={images}
            active={value === Size.L}
            composition={{
              collageType,
              type: compositionType
            }}
            onClick={onSelect(Size.L)}
            label={
              <>
                L Size
                <br />
                74cm x 53cm
              </>
            }
          />
          <WizardThumbnail
            size={Size.XL}
            orientation={Orientation.Portrait}
            images={images}
            active={value === Size.XL}
            composition={{
              collageType,
              type: compositionType
            }}
            onClick={onSelect(Size.XL)}
            label={
              <>
                XL Size
                <br />
                84cm x 60cm
              </>
            }
          />
        </div>
      </Hero>
      <section className="wizard-section">
        <h2>Select poster size</h2>
        <div className="wizard__options">
          <BackButton href="/wizard/poster-type" />
          <WizardOptionButton
            active={value === Size.M}
            onClick={onSelect(Size.M)}
          >
            M size &ndash; 49cm x 35cm
          </WizardOptionButton>
          <WizardOptionButton
            active={value === Size.L}
            onClick={onSelect(Size.L)}
          >
            L size &ndash; 74cm x 53cm
          </WizardOptionButton>
          <WizardOptionButton
            active={value === Size.XL}
            onClick={onSelect(Size.XL)}
          >
            XL size &ndash; 84cm x 60cm
          </WizardOptionButton>
          <NextButton href="/wizard/orientation" />
        </div>
      </section>
    </Layout>
  );
};

export default ChooseSize;
