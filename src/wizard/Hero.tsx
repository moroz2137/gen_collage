import React from "react";

interface Props {
  children?: any;
  simpleViewOnMobile?: boolean;
}

const Hero = ({ children }: Props) => {
  return (
    <section className="pyof-hero">
      <div className="hero__bg" />
      <div className={`container hero__container`}>{children}</div>
    </section>
  );
};

export default Hero;
