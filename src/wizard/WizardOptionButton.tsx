import React from "react";
import Button from "../components/Button";

interface Props {
  onClick(): void;
  children: any;
  active: boolean;
}

const WizardOptionButton = ({ onClick, children, active }: Props) => {
  return (
    <Button
      stroke
      black={!active}
      onClick={onClick}
      className="wizard-option-button"
    >
      {children}
    </Button>
  );
};

export default WizardOptionButton;
