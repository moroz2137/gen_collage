import React from "react";
import useWizardPaths from "./useWizardPaths";
import { ReactComponent as BackIcon } from "../editor/CropImageView/left-arrow.svg";

interface Props {
  href: string;
  children?: string;
}

const BackButton = ({ href, children }: Props) => {
  const navigateWithState = useWizardPaths();
  return (
    <button onClick={() => navigateWithState(href)} className="back-button">
      <BackIcon />
      {children || "Back"}
    </button>
  );
};

export default BackButton;
