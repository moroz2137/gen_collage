import React, { useMemo } from "react";
import { ActionType } from "../store/AppReducerAction";
import { useStateReducer } from "../store/useStateReducer";
import { CompositionType, Orientation, Size } from "../store/AppReducerState";
import Hero from "./Hero";
import Layout from "./Layout";
import CollageTypes from "../store/CollageTypes";
import WizardThumbnail from "./WizardThumbnail";
import JumpingGirls from "../images/jumping-girls.jpg";
import SmilingLabrador from "../images/smiling-labrador.jpg";
import FlyingLabrador from "../images/labrador-w-locie.jpg";
import Corgi from "../images/corgi.jpg";
import PageTitle from "../components/Helmet";
import BackButton from "./BackButton";
import WizardOptionButton from "./WizardOptionButton";
import NextButton from "./NextButton";

const ChooseOrientation = () => {
  const [state, dispatch] = useStateReducer();

  const onSelect = (value: Orientation) => () => {
    dispatch({
      type: ActionType.SetOrientation,
      value
    });
    // navigateWithState("/editor");
  };

  const value = state.orientation;

  const compositionType = state.composition.type;
  const images = useMemo(
    () =>
      compositionType === CompositionType.Collage
        ? [Corgi, FlyingLabrador, SmilingLabrador]
        : [JumpingGirls],
    [compositionType]
  );
  const collageType =
    compositionType === CompositionType.Collage ? CollageTypes[1] : null;

  const composition = { collageType, type: compositionType };

  return (
    <Layout>
      <PageTitle>Select orientation</PageTitle>
      <Hero simpleViewOnMobile>
        <div
          id="chooseOrientation"
          className="hero__images"
          style={{ top: 10 } as React.CSSProperties}
        >
          <WizardThumbnail
            images={images}
            orientation={Orientation.Portrait}
            composition={composition}
            active={value === Orientation.Portrait}
            size={state.size || Size.L}
            label="Portrait"
            onClick={onSelect(Orientation.Portrait)}
          />
          <WizardThumbnail
            images={images}
            orientation={Orientation.Landscape}
            active={value === Orientation.Landscape}
            composition={composition}
            size={state.size || Size.L}
            label="Landscape"
            onClick={onSelect(Orientation.Landscape)}
          />
        </div>
      </Hero>
      <section className="wizard-section">
        <h2>Select orientation</h2>
        <div className="wizard__options">
          <BackButton href="/wizard/size" />
          <WizardOptionButton
            active={value === Orientation.Portrait}
            onClick={onSelect(Orientation.Portrait)}
          >
            Portrait
          </WizardOptionButton>
          <WizardOptionButton
            active={value === Orientation.Landscape}
            onClick={onSelect(Orientation.Landscape)}
          >
            Landscape
          </WizardOptionButton>
          <NextButton href="/editor" />
        </div>
      </section>
    </Layout>
  );
};

export default ChooseOrientation;
