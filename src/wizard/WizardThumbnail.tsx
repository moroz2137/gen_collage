import React from "react";
import {
  CompositionOptions,
  Orientation,
  Size
} from "../store/AppReducerState";
import cs from "classnames";

interface Props {
  size: Size;
  orientation: Orientation;
  composition: CompositionOptions;
  label?: any;
  images: string[];
  active?: boolean;
  onClick?(): void;
  hideOnMobile?: boolean;
}

const WizardThumbnail = ({
  composition,
  orientation,
  label,
  size,
  active,
  hideOnMobile,
  images,
  onClick
}: Props) => {
  const containerClasses = cs({
    "wizard-thumbnail": true,
    "is-active": active,
    landscape: orientation === Orientation.Landscape,
    "is-size-m": size === Size.M,
    "is-size-l": size === Size.L,
    "is-size-xl": size === Size.XL,
    "hide-on-mobile": hideOnMobile,
    "cursor-pointer": !!onClick,
    "cursor-default": !onClick
  });
  return (
    <div className="wizard-preview" onClick={onClick}>
      <div
        className={`${containerClasses} ${
          composition.collageType?.collageClass || ""
        }`}
      >
        {images.map((img, key) => (
          <div key={key} className="thumbnail-cell">
            <img src={img} key={key} alt="" />
          </div>
        ))}
      </div>
      {/* {label ? <p className="label">{label}</p> : null} */}
    </div>
  );
};

export default WizardThumbnail;
