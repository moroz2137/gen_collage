import React from "react";
import { CompositionType, Orientation, Size } from "../store/AppReducerState";
import CollageTypes from "../store/CollageTypes";
import Hero from "./Hero";
import Layout from "./Layout";
import WizardThumbnail from "./WizardThumbnail";
import FlyingLabrador from "../images/labrador-w-locie.jpg";
import Couple from "../images/parka.jpg";
import JumpingGirls from "../images/jumping-girls.jpg";
import SmilingLabrador from "../images/smiling-labrador.jpg";
import { Link } from "react-router-dom";
import BackButton from "./BackButton";

const Landing = () => {
  return (
    <Layout className="landing">
      <Hero>
        <div className="landing__content">
          <BackButton href="/">Home</BackButton>
          <h1>
            Print your own photo
            <br />
            on metal poster
          </h1>
          <Link to="/wizard/poster-type" className="button">
            Get started
          </Link>
        </div>
        <div className="hero__images" id="landingHero">
          <WizardThumbnail
            size={Size.M}
            orientation={Orientation.Landscape}
            images={[FlyingLabrador, Couple, SmilingLabrador]}
            composition={{
              collageType: CollageTypes[1],
              type: CompositionType.Collage
            }}
          />
          <WizardThumbnail
            size={Size.M}
            orientation={Orientation.Portrait}
            images={[JumpingGirls]}
            composition={{
              type: CompositionType.Single,
              collageType: null
            }}
          />
          <WizardThumbnail
            size={Size.M}
            orientation={Orientation.Landscape}
            images={[SmilingLabrador]}
            composition={{
              type: CompositionType.Single,
              collageType: null
            }}
            hideOnMobile
          />
        </div>
      </Hero>
    </Layout>
  );
};

export default Landing;
