import React from "react";
import useWizardPaths from "./useWizardPaths";
import { ReactComponent as NextIcon } from "../editor/CropImageView/left-arrow.svg";

interface Props {
  href: string;
  children?: string;
}

const NextButton = ({ href, children }: Props) => {
  const navigateWithState = useWizardPaths();
  return (
    <button onClick={() => navigateWithState(href)} className="next-button">
      {children || "Next"}
      <NextIcon />
    </button>
  );
};

export default NextButton;
