import React from "react";
import { ActionType } from "../store/AppReducerAction";
import { useStateReducer } from "../store/useStateReducer";
import { CompositionType, Orientation, Size } from "../store/AppReducerState";
import Hero from "./Hero";
import Layout from "./Layout";
import WizardThumbnail from "./WizardThumbnail";
import CollageTypes from "../store/CollageTypes";
import SmilingLabrador from "../images/smiling-labrador.jpg";
import FlyingLabrador from "../images/labrador-w-locie.jpg";
import Corgi from "../images/corgi.jpg";
import PageTitle from "../components/Helmet";
import BackButton from "./BackButton";
import WizardOptionButton from "./WizardOptionButton";
import NextButton from "./NextButton";

const ChooseCompositionType = () => {
  const [state, dispatch] = useStateReducer();

  const value = state.composition.type;

  const onSelect = (value: CompositionType) => () => {
    dispatch({
      type: ActionType.SetCompositionType,
      value
    });
    // navigateWithState("/wizard/size");
  };

  return (
    <Layout>
      <PageTitle>Select poster type</PageTitle>
      <Hero simpleViewOnMobile>
        <div className="hero__images">
          <WizardThumbnail
            size={Size.L}
            label="Collage"
            orientation={Orientation.Landscape}
            images={[Corgi, FlyingLabrador, SmilingLabrador]}
            active={value === CompositionType.Collage}
            composition={{
              collageType: CollageTypes[1],
              type: CompositionType.Collage
            }}
            onClick={onSelect(CompositionType.Collage)}
          />
          <WizardThumbnail
            size={Size.L}
            label="Single photo"
            orientation={Orientation.Landscape}
            images={[FlyingLabrador]}
            active={value === CompositionType.Single}
            onClick={onSelect(CompositionType.Single)}
            composition={{
              collageType: null,
              type: CompositionType.Single
            }}
          />
        </div>
      </Hero>
      <section className="wizard-section">
        <h2>Select poster type</h2>
        <div className="wizard__options">
          <BackButton href="/" />
          <WizardOptionButton
            active={value === CompositionType.Collage}
            onClick={onSelect(CompositionType.Collage)}
          >
            Collage
          </WizardOptionButton>
          <WizardOptionButton
            active={value === CompositionType.Single}
            onClick={onSelect(CompositionType.Single)}
          >
            Single photo
          </WizardOptionButton>
          <NextButton href="/wizard/size" />
        </div>
      </section>
    </Layout>
  );
};

export default ChooseCompositionType;
