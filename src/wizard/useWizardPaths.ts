import { useHistory, useLocation } from "react-router";

export default function useWizardPaths() {
  const history = useHistory();
  const { pathname } = useLocation();

  const navigateWithState = (path: string) => {
    history.push({
      pathname: path,
      state: { previousScreen: pathname }
    });
  };

  return navigateWithState;
}
