import React from "react";

interface Props {
  children: any;
  className?: string;
}

const Layout = ({ children, className }: Props) => {
  return (
    <div className={`layout ${className || ""}`}>
      <main>{children}</main>
      <div className="overlay" />
    </div>
  );
};

export default Layout;
