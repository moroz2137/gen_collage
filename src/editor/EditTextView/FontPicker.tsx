import React from "react";
import style from "../Editor.module.sass";
import { Typeface } from "../../store/AppReducerState";
import { useStateReducer } from "../../store/useStateReducer";
import { ActionType } from "../../store/AppReducerAction";

const OPTIONS = {
  [Typeface.Sans]: "Sans-serif",
  [Typeface.Serif]: "Serif",
  [Typeface.Decorative]: "Cursive",
  [Typeface.Comic]: "Fantasy"
};

const KEYS = (Object.keys(OPTIONS) as unknown) as (keyof typeof OPTIONS)[];

const FontPicker = () => {
  const [state, dispatch] = useStateReducer();

  const selected = state.text.typeface;

  const onChange = (value: Typeface) => () => {
    dispatch({
      type: ActionType.SetTextTypeface,
      value
    });
  };

  return (
    <div className={style.colorPicker}>
      <h2 className={`editor__large_label hide-on-lt-fhd`}>Choose typeface</h2>
      <div className={style.fonts}>
        {KEYS.map((value) => (
          <button
            type="button"
            key={value}
            onClick={onChange(value)}
            className={`has-font-${value} ${style.fontOption} ${
              selected === value ? style.isActive : ""
            }`}
          >
            {OPTIONS[value]}
          </button>
        ))}
      </div>
    </div>
  );
};

export default FontPicker;
