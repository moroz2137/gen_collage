import React from "react";
import ColorPicker from "../../components/ColorPicker";
import { ActionType } from "../../store/AppReducerAction";
import { useStateReducer } from "../../store/useStateReducer";

const OPTIONS = {
  White: "#ffffff",
  Black: "#000000",
  Blue: "#3498db",
  Cyan: "#81ecec",
  Turquoise: "#1abc9c",
  Yellow: "#f1c40f",
  Crimson: "#d63031",
  Purple: "#8e44ad"
};

const TextColorPicker = () => {
  const [state, dispatch] = useStateReducer();
  const selected = state.text.color || null;

  const onChange = (value: string) => () => {
    dispatch({
      type: ActionType.SetTextColor,
      value
    });
  };

  return (
    <ColorPicker
      value={selected}
      onChange={onChange}
      options={OPTIONS}
      label="Choose text colour"
      labelClass="hide-on-lt-fhd"
    />
  );
};

export default TextColorPicker;
