import React, { useCallback } from "react";
import style from "../Editor.module.sass";
import { TextAlignment } from "../../store/AppReducerState";
import { useStateReducer } from "../../store/useStateReducer";
import { ActionType } from "../../store/AppReducerAction";
import { ReactComponent as LeftIcon } from "./left-align.svg";
import { ReactComponent as CenterIcon } from "./centering.svg";
import { ReactComponent as RightIcon } from "./right-align.svg";

const AlignmentPicker = () => {
  const [state, dispatch] = useStateReducer();

  const selected = state.text.alignment;

  const Option = useCallback(
    ({ value, icon: Icon }: any) => {
      const onChange = (value: TextAlignment) => () => {
        dispatch({
          type: ActionType.SetTextAlignment,
          value
        });
      };

      const active = selected === value;
      return (
        <button
          className={`${style.alignmentOption} ${active ? style.isActive : ""}`}
          onClick={onChange(value)}
        >
          <Icon />
        </button>
      );
    },
    [selected, dispatch]
  );

  return (
    <div className={style.alignmentPicker}>
      <h2 className={`editor__large_label hide-on-lt-fhd`}>Text alignment</h2>
      <div className={style.textAlignment}>
        <Option value={TextAlignment.Left} icon={LeftIcon} />
        <Option value={TextAlignment.Center} icon={CenterIcon} />
        <Option value={TextAlignment.Right} icon={RightIcon} />
      </div>
    </div>
  );
};

export default AlignmentPicker;
