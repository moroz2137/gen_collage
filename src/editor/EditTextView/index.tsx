import React, { ChangeEvent } from "react";
import EditorLayout from "../EditorLayout";
import Sidebar from "../Sidebar";
import style from "../Editor.module.sass";
import TextColorPicker from "./TextColorPicker";
import { ActionType } from "../../store/AppReducerAction";
import FontPicker from "./FontPicker";
import AlignmentPicker from "./AlignmentPicker";
import useResettableState from "../useResettableState";
import EditorActions from "../EditorActions";
import { ReactComponent as TextIcon } from "../EditorMainMenu/text-white.svg";
import SidebarSection from "../SidebarSection";

const EditTextView = () => {
  const { state, dispatch, onReset } = useResettableState("text");

  const { content } = state.text || {};

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    dispatch({
      type: ActionType.SetTextContent,
      value
    });
  };

  return (
    <EditorLayout>
      <Sidebar>
        <div className="sidebar-container">
          <SidebarSection icon={TextIcon} pullUp to="/editor" active>
            Add text
          </SidebarSection>
          <label htmlFor="textContent" className={style.largeLabel}>
            Type your text
          </label>
          <input
            type="text"
            autoFocus
            name="textContent"
            className={style.roundedInput}
            onChange={onChange}
            value={content || ""}
          />
          <TextColorPicker />
          <FontPicker />
          <AlignmentPicker />
          <EditorActions onReset={onReset} />
        </div>
      </Sidebar>
    </EditorLayout>
  );
};

export default EditTextView;
