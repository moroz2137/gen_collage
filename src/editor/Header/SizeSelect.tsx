import React, { ChangeEvent, useCallback } from "react";
import { SizeKeys, SizeOptions } from "./size";
import { ActionType } from "../../store/AppReducerAction";
import { useStateReducer } from "../../store/useStateReducer";
import { Size } from "../../store/AppReducerState";
import style from "./Header.module.sass";

interface Props {
  name?: string;
}

const SizeSelect = ({ name }: Props) => {
  const [state, dispatch] = useStateReducer();

  name = name || "sizeSelect";
  const value = state.size || undefined;

  const onChange = useCallback(
    (e: ChangeEvent<HTMLSelectElement>) => {
      const newValue = e.target.value;
      dispatch({
        type: ActionType.SetSize,
        value: newValue as Size
      });
    },
    [dispatch]
  );

  return (
    <div className={style.select}>
      <select name={name} id={name} value={value} onChange={onChange}>
        {SizeKeys.map((key: Size) => (
          <option key={key} value={key}>
            {SizeOptions[key]}
          </option>
        ))}
      </select>
      <label htmlFor={name}>
        <span>Size:</span>
        <strong>{(value && SizeOptions[value]) || "Please select"}</strong>
      </label>
    </div>
  );
};

export default SizeSelect;
