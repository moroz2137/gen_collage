import { CompositionType } from "../../store/AppReducerState";

export const CompositionTypeOptions: Record<CompositionType, string> = {
  [CompositionType.Single]: "Single photo",
  [CompositionType.Collage]: "Collage"
};

export const CompositionTypeKeys: CompositionType[] = (Object.keys(
  CompositionTypeOptions
) as unknown) as CompositionType[];
