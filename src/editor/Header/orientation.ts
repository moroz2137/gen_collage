import { Orientation } from "../../store/AppReducerState";

export const OrientationOptions: Record<Orientation, string> = {
  [Orientation.Landscape]: "Landscape",
  [Orientation.Portrait]: "Portrait"
};

export const OrientationKeys: Orientation[] = (Object.keys(
  OrientationOptions
) as unknown) as Orientation[];
