import React from "react";
import CompositionTypeSelect from "./CompositionTypeSelect";
import OrientationSelect from "./OrientationSelect";
import SizeSelect from "./SizeSelect";
import style from "./Header.module.sass";

const Header = () => {
  return (
    <header className={style.header}>
      <div className={style.wrapper}>
        <SizeSelect />
        <CompositionTypeSelect />
        <OrientationSelect />
      </div>
    </header>
  );
};

export default Header;
