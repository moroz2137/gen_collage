import { Size } from "../../store/AppReducerState";

export const SizeOptions: Record<Size, string> = {
  [Size.M]: "M - 46cm x 32cm",
  [Size.L]: "L - 71cm x 50cm",
  [Size.XL]: "XL - 81cm x 57cm"
};

export const SizePrices: Record<Size, number> = {
  [Size.M]: 39,
  [Size.L]: 79,
  [Size.XL]: 119
};

export const SizeKeys: Size[] = (Object.keys(SizeOptions) as unknown) as Size[];
