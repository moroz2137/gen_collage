import React, { ChangeEvent, useCallback } from "react";
import { CompositionTypeKeys, CompositionTypeOptions } from "./compositionType";
import { ActionType } from "../../store/AppReducerAction";
import { useStateReducer } from "../../store/useStateReducer";
import { CompositionType } from "../../store/AppReducerState";
import style from "./Header.module.sass";

interface Props {
  name?: string;
}

const CompositionTypeSelect = ({ name }: Props) => {
  const [state, dispatch] = useStateReducer();

  name = name || "compositionTypeSelect";
  const value = state.composition.type || undefined;

  const onChange = useCallback(
    (e: ChangeEvent<HTMLSelectElement>) => {
      const newValue = e.target.value;
      dispatch({
        type: ActionType.SetCompositionType,
        value: newValue as CompositionType
      });
    },
    [dispatch]
  );

  return (
    <div className={style.select}>
      <select name={name} id={name} value={value} onChange={onChange}>
        {CompositionTypeKeys.map((key) => (
          <option key={key} value={key}>
            {CompositionTypeOptions[key]}
          </option>
        ))}
      </select>
      <label htmlFor={name}>
        <span>Type:</span>
        <strong>
          {(value && CompositionTypeOptions[value]) || "Please select"}
        </strong>
      </label>
    </div>
  );
};

export default CompositionTypeSelect;
