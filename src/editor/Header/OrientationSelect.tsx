import React, { ChangeEvent, useCallback } from "react";
import { OrientationKeys, OrientationOptions } from "./orientation";
import { ActionType } from "../../store/AppReducerAction";
import { useStateReducer } from "../../store/useStateReducer";
import { Orientation } from "../../store/AppReducerState";
import style from "./Header.module.sass";

interface Props {
  name?: string;
}

const OrientationSelect = ({ name }: Props) => {
  const [state, dispatch] = useStateReducer();

  name = name || "orientationSelect";
  const value = state.orientation || undefined;

  const onChange = useCallback(
    (e: ChangeEvent<HTMLSelectElement>) => {
      const newValue = e.target.value;
      dispatch({
        type: ActionType.SetOrientation,
        value: newValue as Orientation
      });
    },
    [dispatch]
  );

  return (
    <div className={style.select}>
      <select name={name} id={name} value={value} onChange={onChange}>
        {OrientationKeys.map((key: Orientation) => (
          <option key={key} value={key}>
            {OrientationOptions[key]}
          </option>
        ))}
      </select>
      <label htmlFor={name}>
        <span>Orientation:</span>
        <strong>
          {(value && OrientationOptions[value]) || "Please select"}
        </strong>
      </label>
    </div>
  );
};

export default OrientationSelect;
