import React from "react";
import Button from "../components/Button";

interface Props {
  onReset(): void;
}

export default ({ onReset }: Props) => {
  return (
    <div className="action-buttons">
      <Button onClick={onReset} stroke>
        Cancel
      </Button>
      <Button href="/editor" stroke>
        Done
      </Button>
    </div>
  );
};
