import React from "react";
import style from "./ImageFilters.module.sass";
import PreviewImage from "./preview.jpg";
import cs from "classnames";
import useFilterStyles from "../../filters/useFilterStyles";

interface Props {
  active: boolean;
  onSelect(): void;
  preset: string | null;
}

const capitalize = (string: string) => {
  if (typeof string !== "string") return "";
  return string.charAt(0).toUpperCase() + string.slice(1);
};

const FilterPreview = React.memo(({ preset, active, onSelect }: Props) => {
  const { overlay, filter } = useFilterStyles(preset);
  const displayName = capitalize(preset || "None");
  return (
    <div
      className={cs({
        [style.filterOptionPreview]: true,
        [style.isActive]: active
      })}
      title={displayName}
    >
      <div className={style.filterOptionPreviewImg} onClick={onSelect}>
        <img src={PreviewImage} alt={displayName} style={filter} />
        <div style={overlay} />
      </div>
      <p className={style.filterName}>{displayName}</p>
    </div>
  );
});

export default FilterPreview;
