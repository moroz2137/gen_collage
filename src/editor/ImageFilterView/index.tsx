import React from "react";
import { ActionType } from "../../store/AppReducerAction";
import FilterPresets from "../../filters/presets";
import EditorActions from "../EditorActions";
import EditorLayout from "../EditorLayout";
import Sidebar from "../Sidebar";
import useResettableState from "../useResettableState";
import style from "./ImageFilters.module.sass";
import { ReactComponent as FilterIcon } from "../EditorMainMenu/picture-white.svg";
import FilterPreview from "./FilterPreview";
import SidebarSection from "../SidebarSection";

const FilterView = () => {
  const { state, dispatch, onReset } = useResettableState("filter");
  const selected = state.filter;

  const onSelect = (value: string | null) => () => {
    dispatch({
      type: ActionType.SetImageFilter,
      value
    });
  };

  return (
    <EditorLayout>
      <Sidebar>
        <div className="sidebar-container">
          <SidebarSection icon={FilterIcon} to="/editor" active>
            Filters
          </SidebarSection>
          <div className={style.filters}>
            <FilterPreview
              preset={null}
              active={!selected}
              onSelect={onSelect(null)}
            />
            {Object.keys(FilterPresets).map((key) => {
              return (
                <FilterPreview
                  key={key}
                  preset={key}
                  active={key === selected}
                  onSelect={onSelect(key)}
                />
              );
            })}
          </div>
          <EditorActions onReset={onReset} />
        </div>
      </Sidebar>
    </EditorLayout>
  );
};

export default FilterView;
