import React from "react";
import style from "../Sidebar/Sidebar.module.sass";
import { ReactComponent as TextIcon } from "./text.svg";
import { ReactComponent as FrameIcon } from "./frame.svg";
import { ReactComponent as CollageIcon } from "./grid.svg";
import { ReactComponent as PictureIcon } from "./picture.svg";
import Sidebar from "../Sidebar";
import SidebarSection from "../SidebarSection";

const EditorMainMenu = () => {
  return (
    <Sidebar>
      <nav className={style.menu}>
        <SidebarSection to="/editor/text" icon={TextIcon}>
          Add text
        </SidebarSection>
        <SidebarSection to="/editor/frames/plain" icon={FrameIcon}>
          Frames
        </SidebarSection>
        <SidebarSection to="/editor/collages" icon={CollageIcon}>
          Collages
        </SidebarSection>
        <SidebarSection to="/editor/filters" icon={PictureIcon}>
          Filters
        </SidebarSection>
      </nav>
    </Sidebar>
  );
};

export default EditorMainMenu;
