import { useCallback, useMemo } from "react";
import { useHistory } from "react-router";
import { ActionType } from "../store/AppReducerAction";
import { useStateReducer } from "../store/useStateReducer";
import { AppReducerState } from "../store/AppReducerState";

export default (key: keyof AppReducerState) => {
  const [state, dispatch] = useStateReducer();
  const history = useHistory();

  /* eslint react-hooks/exhaustive-deps: 0 */
  const initialState = useMemo(() => state[key], []);

  const onReset = useCallback(() => {
    dispatch({
      type: ActionType.ResetSettings,
      key,
      value: initialState
    });
    history.push("/editor");
  }, [dispatch, initialState]);

  return {
    state,
    dispatch,
    onReset
  };
};
