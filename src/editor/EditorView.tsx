import React from "react";
import EditorLayout from "./EditorLayout";
import EditorMainMenu from "./EditorMainMenu";

const EditorView = () => {
  return (
    <EditorLayout>
      <EditorMainMenu />
    </EditorLayout>
  );
};

export default EditorView;
