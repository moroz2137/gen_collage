import { useCallback, useEffect, useState } from "react";
import { set, get, del } from "idb-keyval";

const STORAGE_KEY_PREFIX = "__PYOF_CROPPED_PREVIEW";

export default (index: number | string) => {
  const key = `${STORAGE_KEY_PREFIX}.${index}`;
  const [preview, setPreview] = useState<any>(null);

  const resetCache = useCallback(async () => {
    const preview = await get(key);
    setPreview(preview);
  }, [key]);

  useEffect(() => {
    resetCache();
  }, [resetCache]);

  const savePreview = async (preview: string) => {
    await set(key, preview);
    resetCache();
  };

  const clearPreview = async () => {
    await del(key);
    resetCache();
  };

  return {
    key,
    preview,
    savePreview,
    clearPreview
  };
};
