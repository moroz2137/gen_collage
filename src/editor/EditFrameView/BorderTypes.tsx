import React from "react";
import style from "../Editor.module.sass";
import { NavLink } from "react-router-dom";

const BorderTypes = () => {
  return (
    <div className={style.borderTypes}>
      <NavLink activeClassName={style.isActive} to="/editor/frames/plain">
        Plain
      </NavLink>
      <NavLink activeClassName={style.isActive} to="/editor/frames/custom">
        Custom
      </NavLink>
    </div>
  );
};

export default BorderTypes;
