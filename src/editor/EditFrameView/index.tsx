import React from "react";
import EditorLayout from "../EditorLayout";
import Sidebar from "../Sidebar";
import useResettableState from "../useResettableState";
import EditorActions from "../EditorActions";
import BorderTypes from "./BorderTypes";
import BorderColorPicker from "./BorderColorPicker";
import { ReactComponent as FrameIcon } from "../EditorMainMenu/frame-white.svg";
import SidebarSection from "../SidebarSection";

const EditFrameView = () => {
  const { onReset } = useResettableState("border");

  return (
    <EditorLayout>
      <Sidebar>
        <div className="sidebar-container">
          <SidebarSection icon={FrameIcon} pullUp to="/editor" active>
            Frames
          </SidebarSection>
          <BorderTypes />
          <div className="flex-1-on-desktop">
            <BorderColorPicker />
          </div>
          <EditorActions onReset={onReset} />
        </div>
      </Sidebar>
    </EditorLayout>
  );
};

export default EditFrameView;
