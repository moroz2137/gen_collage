import React from "react";
import ColorPicker from "../../components/ColorPicker";
import { ActionType } from "../../store/AppReducerAction";
import { BorderType } from "../../store/AppReducerState";
import { useStateReducer } from "../../store/useStateReducer";

const OPTIONS = {
  White: "#ffffff",
  Black: "#000000",
  Gray: "#888888",
  Brown: "#6f0000",
  Beige: "#fed39f",
  "Light Salmon": "#fe8761",
  Orange: "#cf7500"
};

const BorderColorPicker = () => {
  const [state, dispatch] = useStateReducer();
  const selected = state.border.fill;

  const onChange = (value: string) => () => {
    dispatch({
      type: ActionType.SetFrame,
      value: {
        type: BorderType.Plain,
        fill: value
      }
    });
  };

  return (
    <ColorPicker
      value={selected}
      onChange={onChange}
      options={OPTIONS}
      label="Choose border colour"
    />
  );
};

export default BorderColorPicker;
