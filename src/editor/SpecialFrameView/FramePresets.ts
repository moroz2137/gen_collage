import FernPortrait from "../../images/fern-portrait.png";
import FernLandscape from "../../images/fern-landscape.png";
import ViolinPortrait from "../../images/violin-portrait.png";
import ViolinLandscape from "../../images/violin-landscape.png";

export interface FramePreset {
  name: string;
  images: {
    portrait: string;
    landscape: string;
  };
}

export const FramePresets: Record<string, FramePreset> = {
  fern: {
    name: "Fern",
    images: {
      portrait: FernPortrait,
      landscape: FernLandscape
    }
  },
  violins: {
    name: "Violins 'n' Roses",
    images: {
      portrait: ViolinPortrait,
      landscape: ViolinLandscape
    }
  }
};
