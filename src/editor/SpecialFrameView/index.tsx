import React from "react";
import EditorLayout from "../EditorLayout";
import Sidebar from "../Sidebar";
import { ReactComponent as FrameIcon } from "../EditorMainMenu/frame-white.svg";
import { FramePresets } from "./FramePresets";
import style from "../ImageFilterView/ImageFilters.module.sass";
import { ActionType } from "../../store/AppReducerAction";
import cs from "classnames";
import BorderTypes from "../EditFrameView/BorderTypes";
import { BorderType } from "../../store/AppReducerState";
import useResettableState from "../useResettableState";
import EditorActions from "../EditorActions";
import SidebarSection from "../SidebarSection";

const SpecialFrameView = () => {
  const { onReset, dispatch, state } = useResettableState("border");
  const selected = state.border.fill;
  const onSelect = (key: string) => () => {
    dispatch({
      type: ActionType.SetFrame,
      value: {
        type: BorderType.Custom,
        fill: key
      }
    });
  };
  return (
    <EditorLayout>
      <Sidebar>
        <div className="sidebar-container">
          <SidebarSection icon={FrameIcon} pullUp to="/editor" active>
            Frames
          </SidebarSection>
          <BorderTypes />
          <div className={style.frames}>
            {Object.keys(FramePresets).map((key) => {
              const preset = FramePresets[key];
              const active = key === selected;
              return (
                <div
                  className={cs({
                    [style.filterOptionPreview]: true,
                    [style.isActive]: active
                  })}
                  onClick={onSelect(key)}
                  title={preset.name}
                  key={key}
                >
                  <div className={style.filterOptionPreviewImg}>
                    <img src={preset.images.portrait} alt={preset.name} />
                  </div>
                  <p className={style.filterName}>{preset.name}</p>
                </div>
              );
            })}
          </div>
          <EditorActions onReset={onReset} />
        </div>
      </Sidebar>
    </EditorLayout>
  );
};

export default SpecialFrameView;
