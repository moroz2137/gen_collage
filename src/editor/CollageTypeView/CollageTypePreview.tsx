import React from "react";
import { useStateReducer } from "../../store/useStateReducer";
import { Orientation } from "../../store/AppReducerState";
import { CollageType } from "../../store/CollageTypes";
import useCroppedPreview from "../useCroppedPreview";

interface Props {
  type: CollageType;
  onSelect(): void;
}

const CollageImagePreview = ({ images, index }: any) => {
  const { preview } = useCroppedPreview(index);
  if (!images[index]) return null;
  return <img src={preview} alt="" />;
};

const CollageTypePreview = React.memo(({ type, onSelect }: Props) => {
  const [state] = useStateReducer();
  const images = state.images;
  const orientation = state.orientation;
  const { imageCount, name, collageClass } = type;
  return (
    <div className="collage-type-preview">
      <div
        className={`collage-type-thumbnail ${collageClass} ${
          orientation === Orientation.Landscape ? `landscape` : ""
        }`}
        onClick={onSelect}
      >
        {new Array(imageCount).fill(null).map((_: any, index) => (
          <div key={index} className="thumbnail-cell">
            <CollageImagePreview images={images} index={index} />
          </div>
        ))}
      </div>
      <p className="thumbnail__caption">{name}</p>
    </div>
  );
});

export default CollageTypePreview;
