import React from "react";
import EditorLayout from "../EditorLayout";
import Sidebar from "../Sidebar";
import { ReactComponent as GridIcon } from "../EditorMainMenu/grid-white.svg";
import CollageTypes, { CollageType } from "../../store/CollageTypes";
import CollageTypePreview from "./CollageTypePreview";
import useResettableState from "../useResettableState";
import EditorActions from "../EditorActions";
import { ActionType } from "../../store/AppReducerAction";
import SidebarSection from "../SidebarSection";

const CollageTypeView = () => {
  const { dispatch, onReset } = useResettableState("composition");

  const onSelect = (value: CollageType) => () => {
    dispatch({
      type: ActionType.SetCollageType,
      value
    });
  };

  return (
    <EditorLayout>
      <Sidebar>
        <div className="sidebar-container">
          <SidebarSection icon={GridIcon} to="/editor" active>
            Collages
          </SidebarSection>
          <div className="collage-types">
            {CollageTypes.map((type, index) => (
              <CollageTypePreview
                type={type}
                key={index}
                onSelect={onSelect(type)}
              />
            ))}
          </div>
          <EditorActions onReset={onReset} />
        </div>
      </Sidebar>
    </EditorLayout>
  );
};

export default CollageTypeView;
