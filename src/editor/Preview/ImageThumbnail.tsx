import React, { HTMLProps, MouseEvent, useRef } from "react";
import { Link } from "react-router-dom";
import style from "./Preview.module.sass";
import { ReactComponent as CropIcon } from "./crop.svg";
import { useStateReducer } from "../../store/useStateReducer";
import { ActionType } from "../../store/AppReducerAction";

interface Props extends HTMLProps<HTMLImageElement> {
  index: number;
}

const ImageThumbnail = ({ index, style: filterStyle, src }: Props) => {
  const ref = useRef<HTMLImageElement>(null);
  const [, dispatch] = useStateReducer();
  const onClick = (e: MouseEvent<HTMLAnchorElement>) => {
    e.stopPropagation();
    if (!ref.current) return;
    const image = ref.current;
    dispatch({
      type: ActionType.SetAspectRatio,
      index,
      value: image.clientWidth / image.clientHeight
    });
  };
  return (
    <>
      <img src={src} alt="" style={filterStyle} ref={ref} />
      <Link
        to={`/editor/crop/${index}`}
        className={style.cropButton}
        onClick={onClick}
        title="Recrop"
      >
        <CropIcon />
      </Link>
    </>
  );
};

export default ImageThumbnail;
