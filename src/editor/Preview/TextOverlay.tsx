import React from "react";
import { useStateReducer } from "../../store/useStateReducer";
import style from "./Preview.module.sass";

const TextOverlay = () => {
  const [state] = useStateReducer();

  const { content, color, alignment, typeface } = state.text || {};

  if (!content) return null;

  return (
    <h1
      className={`${style.textOverlay} has-font-${typeface} text-${alignment}`}
      style={{ color: color || "#000" }}
    >
      {content}
    </h1>
  );
};

export default TextOverlay;
