import React, { useEffect } from "react";
import { useStateReducer } from "../../store/useStateReducer";
import style from "./Preview.module.sass";
import cs from "classnames";
import {
  BorderType,
  CompositionType,
  Orientation,
  Size
} from "../../store/AppReducerState";
import PreviewImage from "./PreviewImage";
import { ActionType } from "../../store/AppReducerAction";
import TextOverlay from "./TextOverlay";
import useFilterStyles from "../../filters/useFilterStyles";
import { FramePresets } from "../SpecialFrameView/FramePresets";

const Preview = () => {
  const [state, dispatch] = useStateReducer();

  const orientation = state.orientation;
  const size = state.size;
  const composition = state.composition;
  const borderType = state.border.type;
  const borderColor =
    borderType === BorderType.Plain ? state.border.fill : null;
  const images = state.images;
  const displayImageCount =
    composition.type === CompositionType.Single
      ? 1
      : composition.collageType?.imageCount;
  const customBorder =
    borderType === BorderType.Custom && state.border.fill
      ? FramePresets[state.border.fill]
      : null;

  useEffect(() => {
    if (Number(displayImageCount) > images.length) {
      dispatch({
        type: ActionType.ExpandEmptySlots,
        value: Number(displayImageCount)
      });
    }
  }, [displayImageCount, images.length, dispatch]);

  const previewClasses = cs({
    [style.preview]: true,
    [style.landscape]: orientation === Orientation.Landscape,
    landscape: orientation === Orientation.Landscape,
    [style.portrait]: orientation === Orientation.Portrait,
    portrait: orientation === Orientation.Portrait,
    [style.collage]: composition.type === CompositionType.Collage,
    [style.single]: composition.type === CompositionType.Single,
    [style.sizeM]: size === Size.M,
    [style.sizeL]: size === Size.L,
    [style.sizeXL]: size === Size.XL,
    [style.plainBorder]: borderType === BorderType.Plain,
    [style.specialBorder]: borderType === BorderType.Custom
  });

  const css = {
    "--border-fill": borderColor
  } as React.CSSProperties;

  const { filter, overlay } = useFilterStyles(state.filter);

  return (
    <div className={previewClasses} style={css}>
      <div
        className={`${style.previewFrame} ${
          composition.type === CompositionType.Collage
            ? composition.collageType?.collageClass
            : ""
        }`}
      >
        {images.map((image, index) =>
          displayImageCount && index < displayImageCount ? (
            <PreviewImage
              index={index}
              key={index}
              image={image}
              filter={filter}
            />
          ) : null
        )}
        <div style={overlay} className={style.overlay} />
        {customBorder ? (
          <img
            src={
              orientation === Orientation.Portrait
                ? customBorder.images.portrait
                : customBorder.images.landscape
            }
            alt=""
            className={style.customBorderOverlay}
          />
        ) : null}
        <TextOverlay />
      </div>
    </div>
  );
};

export default Preview;
