import React from "react";
import { ReactComponent as UploadIcon } from "./upload.svg";

interface Props {
  progress: number | null;
}

const UploadPlaceholder = ({ progress }: Props) => {
  const isTouchScreen = "ontouchstart" in window;
  return (
    <>
      <UploadIcon />
      {progress ? null : (
        <p>
          {isTouchScreen ? (
            <>
              Tap here
              <br />
              to upload an image
            </>
          ) : (
            <>
              Click here
              <br />
              or drop an image to upload
            </>
          )}
        </p>
      )}
      <progress value={String(progress || 0)} />
    </>
  );
};

export default UploadPlaceholder;
