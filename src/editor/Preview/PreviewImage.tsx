import React, { CSSProperties, useCallback, useRef, useState } from "react";
import { useDropzone, FileRejection } from "react-dropzone";
import { ActionType } from "../../store/AppReducerAction";
import { useStateReducer } from "../../store/useStateReducer";
import { Image } from "../../store/AppReducerState";
import { uploadFile } from "../../upload/ImageUploader";
import style from "./Preview.module.sass";
import useCroppedPreview from "../useCroppedPreview";
import { useHistory } from "react-router";
import UploadPlaceholder from "./UploadPlaceholder";
import ImageThumbnail from "./ImageThumbnail";

interface Props {
  index: number;
  image?: Image | null;
  filter?: CSSProperties;
}

const ACCEPTED_FILETYPES = ["image/png", "image/jpeg"];

const PreviewImage = ({ index, image, filter }: Props) => {
  const [, dispatch] = useStateReducer();
  const { preview } = useCroppedPreview(index);
  const [progress, setProgress] = useState<number | null>(null);
  const containerRef = useRef<HTMLDivElement>(null);
  const history = useHistory();
  const onDrop = useCallback(
    async (acceptedFiles: File[], rejectedFiles: FileRejection[]) => {
      const container = containerRef.current;
      const file = acceptedFiles[0];
      if (file) setProgress(0);
      if (rejectedFiles.length) {
        dispatch({
          type: ActionType.SetRejectedFiles,
          value: rejectedFiles
        });
        return;
      }
      const url = await uploadFile(file, "images", setProgress);
      dispatch({
        type: ActionType.UploadFinished,
        index,
        image: {
          remoteURL: url,
          displayName: file.name,
          cropData: null,
          aspectRatio:
            container && container.clientWidth / container.clientHeight
        }
      });
      setProgress(null);
      history.push(`/editor/crop/${index}`);
    },
    [dispatch, index, history]
  );

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: ACCEPTED_FILETYPES,
    maxSize: 10485760
  });
  return (
    <div
      {...getRootProps()}
      className={image && !progress ? style.imageThumbnail : style.placeholder}
      ref={containerRef}
    >
      <input {...getInputProps()} />
      {image && !progress ? (
        <ImageThumbnail
          src={preview || image.remoteURL}
          alt=""
          style={filter}
          index={index}
        />
      ) : (
        <UploadPlaceholder progress={progress} />
      )}
    </div>
  );
};

export default PreviewImage;
