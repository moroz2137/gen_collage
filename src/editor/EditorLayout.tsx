import React from "react";
import Header from "./Header";
import style from "./Editor.module.sass";
import Preview from "./Preview";
import cs from "classnames";
import { useStateReducer } from "../store/useStateReducer";
import FileRejectedModal from "../components/FileRejectedModal";

interface Props {
  children: any;
  hidePreviewOnMobile?: boolean;
}

const EditorLayout = ({ children, hidePreviewOnMobile }: Props) => {
  const [state] = useStateReducer();
  const showModal = state.rejectedFiles?.length > 0;
  return (
    <div className={style.editor}>
      <Header />
      <main className={style.main}>
        <div
          className={cs({
            [style.preview]: true,
            "hide-on-mobile": hidePreviewOnMobile
          })}
        >
          <Preview />
        </div>
        {children}
      </main>
      <div className={`overlay ${showModal ? "is-visible" : ""}`} />
      {showModal ? <FileRejectedModal /> : null}
    </div>
  );
};

export default EditorLayout;
