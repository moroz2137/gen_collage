import React from "react";
import { Link } from "react-router-dom";
import { CropState } from ".";
import style from "./CropperImageView.module.sass";

interface Props {
  state: CropState;
  progress?: number;
}

const Loader = React.memo(({ state, progress }: Props) => {
  switch (state) {
    case CropState.Ready:
      return null;

    case CropState.Error:
      return (
        <div className={style.loader}>
          <h1>Download failed</h1>
          <p>
            We could not download the image at the moment. Please try refreshing
            the page or reuploading the file if the error persists.
          </p>
          <Link to="/editor" className="link-back">
            &lt;&lt; Go back
          </Link>
        </div>
      );

    case CropState.Loading: {
      const displayProgress = typeof progress === "number";
      return (
        <div className={style.loader}>
          <h1>Loading</h1>
          <p>Your image is downloading. This may take some time.</p>
          {displayProgress ? <progress value={progress} /> : null}
        </div>
      );
    }

    case CropState.Initializing:
      return (
        <div className={style.loader}>
          <h1>Initializing</h1>
          <p>The cropper widget is initializing. This may take some time.</p>
        </div>
      );
  }
});

export default Loader;
