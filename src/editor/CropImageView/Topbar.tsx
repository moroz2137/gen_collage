import React from "react";
import { Link } from "react-router-dom";
import style from "../Header/Header.module.sass";
import { ReactComponent as BackIcon } from "./left-arrow.svg";

interface Props {
  onCrop(): void;
}

const Topbar = ({ onCrop }: Props) => {
  return (
    <header className={style.cropHeader}>
      <Link to="/editor" className={style.backLink}>
        <BackIcon />
        Back
      </Link>
      <div className={style.explanationText}>
        Please crop the image and press Enter. You can repeat this step by
        clicking the icon on the image in the editor view.
      </div>
      <button
        type="button"
        onClick={onCrop}
        className={`${style.confirmButton} button stroke`}
      >
        Crop
      </button>
    </header>
  );
};

export default Topbar;
