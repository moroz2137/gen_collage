import React, { useEffect, useRef, useState, useCallback } from "react";
import Cropper from "cropperjs";
import "cropperjs/dist/cropper.css";
import { useStateReducer } from "../../store/useStateReducer";
import { useHistory, useParams } from "react-router";
import style from "./CropperImageView.module.sass";
import useCroppedPreview from "../useCroppedPreview";
import { ActionType } from "../../store/AppReducerAction";
import Loader from "./Loader";
import Topbar from "./Topbar";
import { downloadFile } from "../../upload/ImageUploader";

interface CropperParams {
  index: string;
}

export enum CropState {
  Loading,
  Initializing,
  Ready,
  Error
}

const CropImageView = () => {
  const { index } = useParams<CropperParams>();
  const [state, dispatch] = useStateReducer();
  const [loading, setLoading] = useState<CropState>(CropState.Loading);
  const [progress, setProgress] = useState<number>();
  const image = state.images[Number(index)];
  const history = useHistory();
  const cropperRef = useRef<Cropper>();
  const imgRef = useRef<HTMLImageElement | null>(null);

  const saveCropData = (data: Cropper.Data) => {
    dispatch({
      type: ActionType.SetCropData,
      index: Number(index),
      data
    });
  };

  const handleSave = () => {
    const cropper = cropperRef.current;
    if (!cropper) return;
    savePreview(
      cropper.getCroppedCanvas({ maxHeight: 800, maxWidth: 800 }).toDataURL()
    );
    const cropData = cropper.getData();
    saveCropData(cropData);
    history.push("/editor");
  };

  const onError = useCallback(() => {
    setLoading(CropState.Error);
  }, [setLoading]);

  useEffect(() => {
    if (!image || !image?.remoteURL) history.push("/editor");

    downloadFile((image as any).remoteURL, setProgress)
      .then((img: any) => {
        if (!imgRef.current) return;
        console.log(img);
        if (imgRef.current) {
          imgRef.current.src = img;
        }
        setLoading(CropState.Initializing);
        cropperRef.current = new Cropper(imgRef.current, {
          aspectRatio: image?.aspectRatio || undefined,
          ready() {
            setLoading(CropState.Ready);
          }
        });
      })
      .catch(onError);
  }, [history, image, onError]);

  useEffect(() => {
    const handleKey = (e: KeyboardEvent) => {
      if (e.key === "Enter") {
        handleSave();
      }
    };

    window.addEventListener("keydown", handleKey);

    return () => window.removeEventListener("keydown", handleKey);
  });

  const { savePreview } = useCroppedPreview(index);

  return (
    <div className={style.layout}>
      <Topbar onCrop={handleSave} />
      <div className={style.cropper}>
        <img
          crossOrigin="anonymous"
          alt=""
          ref={imgRef}
          className={style.image}
        />
        <Loader state={loading} progress={progress} />
      </div>
    </div>
  );
};

export default CropImageView;
