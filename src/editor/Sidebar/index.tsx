import React from "react";
import Button from "../../components/Button";
import style from "./Sidebar.module.sass";
import { SizePrices } from "../Header/size";
import { useStateReducer } from "../../store/useStateReducer";
import { useAddToCart } from "../../store/useAddToCart";

interface Props {
  children: any;
}

export default ({ children }: Props) => {
  const [state] = useStateReducer();

  const { addToCart } = useAddToCart();

  const size = state.size;

  return (
    <div className={style.sidebar}>
      {children}
      <section className={style.checkoutSection}>
        <div className={style.priceRow}>
          <p className={style.priceRowLabel}>Price:</p>
          <p className={style.priceRowAmount}>
            {size && `$${SizePrices[size]}`}
          </p>
        </div>
        <Button fullwidth onClick={addToCart}>
          Go to cart
        </Button>
      </section>
    </div>
  );
};
