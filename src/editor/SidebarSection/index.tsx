import React from "react";
import style from "../Sidebar/Sidebar.module.sass";
import { Link } from "react-router-dom";
import cs from "classnames";

interface Props {
  icon: React.FunctionComponent<any>;
  children: string;
  pullUp?: boolean;
  to: string;
  active?: boolean;
}

const SidebarSection = ({
  to,
  children,
  active,
  pullUp,
  icon: Icon
}: Props) => {
  return (
    <Link
      className={cs({
        [style.menuLink]: true,
        [style.pullUp]: pullUp,
        [style.isActive]: active
      })}
      to={to}
    >
      <Icon />
      <span className={style.menuLinkText}>{children}</span>
    </Link>
  );
};

export default SidebarSection;
