import { useContext } from "react";
import { AppReducerContext, AppReducerContextValue } from "./AppReducerContext";

export const useStateReducer = (): AppReducerContextValue => {
  return useContext(AppReducerContext) as AppReducerContextValue;
};
