import { Reducer } from "react";
import { ActionType, AppReducerAction } from "./AppReducerAction";
import { AppReducerState, CompositionType, Image } from "./AppReducerState";
import CollageTypes from "./CollageTypes";

const AppReducer: Reducer<AppReducerState, AppReducerAction> = (
  state,
  action
) => {
  switch (action.type) {
    case ActionType.SetOrientation:
      return {
        ...state,
        orientation: action.value
      };

    case ActionType.SetSize:
      return {
        ...state,
        size: action.value
      };

    case ActionType.SetImageFilter:
      return {
        ...state,
        filter: action.value
      };

    case ActionType.SetAspectRatio: {
      const images = [...state.images];
      const index = action.index;
      if (!images[index]) return state;
      images[index] = {
        ...images[index],
        aspectRatio: action.value
      } as Image;
      return {
        ...state,
        images
      };
    }

    case ActionType.SetCropData: {
      const images = [...state.images];
      const index = action.index;
      if (!images[index]) return state;
      images[index] = {
        ...images[index],
        cropData: action.data
      } as Image;
      return {
        ...state,
        images
      };
    }

    case ActionType.SetCompositionType: {
      let collageType = state.composition?.collageType || null;
      if (!collageType && action.value === CompositionType.Collage) {
        collageType = CollageTypes[0];
      }
      return {
        ...state,
        composition: {
          type: action.value,
          collageType
        }
      };
    }

    case ActionType.SetCollageType: {
      return {
        ...state,
        composition: {
          type: CompositionType.Collage,
          collageType: action.value
        }
      };
    }

    case ActionType.SetTextColor: {
      return {
        ...state,
        text: {
          ...state.text,
          color: action.value
        }
      };
    }

    case ActionType.SetTextContent: {
      return {
        ...state,
        text: {
          ...state.text,
          content: action.value
        }
      };
    }

    case ActionType.SetTextTypeface:
      return {
        ...state,
        text: {
          ...state.text,
          typeface: action.value
        }
      };

    case ActionType.SetTextAlignment:
      return {
        ...state,
        text: {
          ...state.text,
          alignment: action.value
        }
      };

    case ActionType.ExpandEmptySlots: {
      const images = state.images;
      const target = action.value;

      for (let i = 0, n = target - images.length; i < n; i++) {
        images.push(null);
      }
      return {
        ...state,
        images
      };
    }

    case ActionType.SetFrame:
      return {
        ...state,
        border: action.value
      };

    case ActionType.ResetSettings:
      return {
        ...state,
        [action.key]: action.value
      };

    case ActionType.UploadFinished:
      const newImages = state.images;
      newImages[action.index] = action.image;
      return {
        ...state,
        images: newImages
      };

    case ActionType.SetRejectedFiles:
      return {
        ...state,
        rejectedFiles: action.value
      };

    default:
      return state;
  }
};
export default AppReducer;
