import { CollageType } from "./CollageTypes";
import { FileRejection } from "react-dropzone";

export enum Orientation {
  Landscape = "landscape",
  Portrait = "portrait"
}

export enum Size {
  M = "M",
  L = "L",
  XL = "XL"
}

export enum CompositionType {
  Single = "single",
  Collage = "collage"
}

export interface CompositionOptions {
  type: CompositionType;
  collageType: CollageType | null;
}

export enum BorderType {
  Plain,
  Custom
}

export interface BorderOptions {
  type: BorderType;
  fill: string | null;
}

export interface Image {
  remoteURL: string;
  displayName: string | null;
  aspectRatio: number | null;
  cropData: Cropper.Data | null;
}

export enum TextAlignment {
  Left = "left",
  Right = "right",
  Center = "center"
}

export enum Typeface {
  Sans = "sans",
  Serif = "serif",
  Decorative = "decorative",
  Comic = "comic"
}

export interface TextOverlay {
  content: string | null;
  typeface: Typeface | null;
  alignment: TextAlignment;
  color: string | null;
}

export interface AppReducerState {
  orientation: Orientation | null;
  size: Size | null;
  filter: string | null;
  composition: CompositionOptions;
  border: BorderOptions;
  images: (Image | null)[];
  text: TextOverlay;
  rejectedFiles: FileRejection[];
}

export const AppReducerStateFactory = (): Readonly<AppReducerState> => {
  return {
    orientation: null,
    size: null,
    filter: null,
    composition: {
      type: CompositionType.Single,
      collageType: null
    },
    border: {
      type: BorderType.Plain,
      fill: null
    },
    images: new Array(10).fill(null),
    text: {
      content: null,
      color: "#000",
      alignment: TextAlignment.Left,
      typeface: Typeface.Sans
    },
    rejectedFiles: []
  };
};
