import { useStateReducer } from "./useStateReducer";
import {
  CompositionType,
  AppReducerState,
  CompositionOptions
} from "./AppReducerState";

function getImageCount(state: AppReducerState) {
  if (state.composition.type === CompositionType.Single) return 1;
  return state.composition.collageType?.imageCount;
}

function getUsedImages(state: AppReducerState) {
  const count = getImageCount(state);
  return state.images.slice(0, count);
}

function cleanCompositionData(state: AppReducerState): CompositionOptions {
  if (state.composition.type === CompositionType.Single) {
    return {
      type: CompositionType.Single,
      collageType: null
    };
  }
  return state.composition;
}

export const useAddToCart = () => {
  const [state] = useStateReducer();
  const serialize = () => {
    const { rejectedFiles, ...rest } = state;

    return {
      ...rest,
      composition: cleanCompositionData(state),
      images: getUsedImages(state)
    };
  };

  const addToCart = () => {
    const payload = serialize();
    console.log(JSON.stringify(payload, null, 2));
  };

  return {
    serialize,
    addToCart
  };
};
