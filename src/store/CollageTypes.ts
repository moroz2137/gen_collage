export interface CollageType {
  name: string;
  imageCount: number;
  collageClass: string;
}

const CollageTypes: CollageType[] = [
  { name: "Double", imageCount: 2, collageClass: "double" },
  { name: "Triple", imageCount: 3, collageClass: "triple" },
  { name: "Quadruple", imageCount: 4, collageClass: "quadruple" },
  { name: "Six photos", imageCount: 6, collageClass: "six-photos" }
];

export default CollageTypes;
