import {
  AppReducerState,
  BorderOptions,
  CompositionType,
  Image,
  Orientation,
  Size,
  TextAlignment,
  Typeface
} from "./AppReducerState";
import { CollageType } from "./CollageTypes";
import { FileRejection } from "react-dropzone";

export enum ActionType {
  SetOrientation,
  SetSize,
  SetCompositionType,
  SetCollageType,
  SetFrame,
  UploadFinished,
  ExpandEmptySlots,
  SetTextColor,
  SetTextContent,
  SetTextTypeface,
  SetTextAlignment,
  ResetSettings,
  SetImageFilter,
  SetCropData,
  SetAspectRatio,
  SetRejectedFiles
}

interface SetOrientationAction {
  type: ActionType.SetOrientation;
  value: Orientation;
}

interface SetSizeAction {
  type: ActionType.SetSize;
  value: Size;
}

interface SetCompositionTypeAction {
  type: ActionType.SetCompositionType;
  value: CompositionType;
}

interface SetCollageTypeAction {
  type: ActionType.SetCollageType;
  value: CollageType;
}

interface SetFrameAction {
  type: ActionType.SetFrame;
  value: BorderOptions;
}

interface UploadFinishedAction {
  type: ActionType.UploadFinished;
  index: number;
  image: Image;
}

interface ExpandEmptySlotsAction {
  type: ActionType.ExpandEmptySlots;
  value: number;
}

interface SetTextColorAction {
  type: ActionType.SetTextColor;
  value: string;
}

interface SetTextContentAction {
  type: ActionType.SetTextContent;
  value: string;
}

interface SetTextTypefaceAction {
  type: ActionType.SetTextTypeface;
  value: Typeface;
}

interface ChangeTextAlignmentAction {
  type: ActionType.SetTextAlignment;
  value: TextAlignment;
}

interface ResetSettingsAction {
  type: ActionType.ResetSettings;
  key: keyof AppReducerState;
  value: any;
}

interface SetImageFilterAction {
  type: ActionType.SetImageFilter;
  value: string | null;
}

interface SetCropDataAction {
  type: ActionType.SetCropData;
  index: number;
  data: Cropper.Data;
}

interface SetAspectRatioAction {
  type: ActionType.SetAspectRatio;
  index: number;
  value: number | null;
}

interface SetRejectedFilesAction {
  type: ActionType.SetRejectedFiles;
  value: FileRejection[];
}

export type AppReducerAction =
  | SetOrientationAction
  | SetSizeAction
  | SetCompositionTypeAction
  | SetFrameAction
  | SetCollageTypeAction
  | UploadFinishedAction
  | ExpandEmptySlotsAction
  | SetTextColorAction
  | SetTextContentAction
  | SetTextTypefaceAction
  | ChangeTextAlignmentAction
  | ResetSettingsAction
  | SetImageFilterAction
  | SetCropDataAction
  | SetAspectRatioAction
  | SetRejectedFilesAction;
