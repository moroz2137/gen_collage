import React, { useEffect, useReducer } from "react";
import AppReducer from "./AppReducer";
import { AppReducerAction } from "./AppReducerAction";
import { AppReducerState, AppReducerStateFactory } from "./AppReducerState";

export type AppReducerContextValue = [
  Readonly<AppReducerState>,
  React.Dispatch<AppReducerAction>
];

export const AppReducerContext = React.createContext<AppReducerContextValue | null>(
  null
);

// https://stackoverflow.com/questions/54346866/save-to-localstorage-from-reducer-hook
function useLocallyPersistedReducer(
  reducer: typeof AppReducer,
  defaultState: AppReducerState,
  storageKey: string,
  init: any = null
) {
  const hookVars = useReducer(reducer, defaultState, (defaultState) => {
    const json = localStorage.getItem(storageKey);
    const persisted = json && JSON.parse(json);
    if (persisted !== null) {
      persisted.rejectedFiles = [];
    }
    return persisted !== null
      ? persisted
      : init !== null
      ? init(defaultState)
      : defaultState;
  });

  const state = hookVars[0];

  useEffect(() => {
    localStorage.setItem(storageKey, JSON.stringify(state));
  }, [storageKey, state]);

  return hookVars;
}

export const useStateReducerProvider = (): AppReducerContextValue => {
  return useLocallyPersistedReducer(
    AppReducer,
    AppReducerStateFactory(),
    "__PYOF_REDUCER_STATE"
  );
};
