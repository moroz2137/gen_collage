const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  webpack: {
    configure: {
      plugins: [
        new MiniCssExtractPlugin({
          filename: "static/css/[name].css",
          chunkFilename: "static/css/[name].chunk.css"
        })
      ],
      optimization: {
        splitChunks: {
          chunks: "initial",
          minSize: 1000000,
          cacheGroups: {
            default: false
          }
        }
      },
      output: {
        chunkFilename: "static/js/[name].chunk.js",
        filename: "static/js/[name].js"
      }
    }
  }
};
